class Patient < ActiveRecord::Base
  attr_accessible :insurance_id, :name, :street_address
  belongs_to :insurance

  has_many :appointments
  has_many :specialists, :through => :appointments
end
