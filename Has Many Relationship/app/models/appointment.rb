class Appointment < ActiveRecord::Base
  attr_accessible :appointment_date, :complaint, :patient_id, :specialist_id
  belongs_to :patient
  belongs_to :specialist

    def days_until_appointment
      (appointment_date - Date.today).to_int
    end
end
